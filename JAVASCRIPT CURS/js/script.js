var text = "Salut, ce mai faci? Cum mai esti?";
var lungime_text = text.length;
console.log(text.length);

console.log(text.indexOf("ce"));
sub = text.substr(2, 2);
console.log(sub);

var x = 15;
switch (x) {
  case 10:
    console.log("x este 10");
    break;
  case 15:
    console.log("x este 15");
    break;
  case 25:
    console.log("x este 25");
    break;
  default:
    console.log("x nu este unul din numere");
}

cifra1 = 11;
cifra2 = 10;
switch (cifra1) {
  case cifra2:
    console.log("Numerele sunt egale");
    break;
  default:
    if (cifra1 > cifra2) {
      console.log("Numarul mai mare este: " + cifra1);
    } else {
      console.log("Numarul mai mare este: " + cifra2);
    }
}

//f(x) = x + 5

function f(x) {
  var res = x + 5;
  return res;
}
function e(x) {
  return f(x) + 2;
}
//console.log(e(2))

//functia mai_mare(a, b); -> returneze numarul mai mare dintre a si b sau daca sunt egale

function mai_mare(y, z) {
  if (y > z) {
    return y;
  } else if (y < z) {
    return z;
  } else {
    return "Numerele sunt egale";
  }
}
console.log(mai_mare(5, 10));

function arie(forma, a, b, c) {
  var rezultat;
  var pi = 3.14;
  switch (forma) {
    case "triunghi":
      rezultat = (a * b) / 2;
      break;
    case "dreptunghi":
      rezultat = a * b;
      break;
    case "trapez":
      rezultat = ((a + b) * c) / 2;
      break;
    case "cerc":
      rezultat = pi * a * a;
      break;
    default:
      rezultat = "Nu stiu sa calculez aria " + forma;
  }
  return rezultat;
}
console.log("Aria triunghiului este: " + arie("triunghi", 6, 6));
console.log("Aria dreptunghiului este: " + arie("dreptunghi", 6, 6));
console.log("Aria trapezului este: " + arie("trapez", 6, 6, 6));
console.log("Aria cercului este: " + arie("cerc", 6));

for (var i = 1; i <= 100; i++) {
  console.log(i);
}

// sa se afiseze folosind for numerele cuprinse intre numerele cuprinse intre 8 si 130 divizibile cu 7
for (var rez, i = 8; i <= 130; i++) {
  if (i % 7 == 0) console.log(i);
}

//inclus intr-o functie astfel incat sa fie divizibil, a b c, sa afiseze intre parametrul a si b divizibile cu c

function divizibile(a, b, c) {
  for (var h = a; h <= b; h++) {
    if (h % c == 0) {
      console.log(h);
    }
  }
}
divizibile(3, 50, 4);

function multiply(a, b) {
  return a * b;
}
console.log(multiply(7, 6));

i = 1;
while (i <= 100) {
  console.log(i);
  i++;
}

i = 1;
do {
  console.log(i);
  i++;
} while (i <= 100);

// A R R A Y

console.log("ARRAY");

var ar = [0, 5, 8, 24, 75, "salut", true];
console.log(ar[1]);
console.log(ar);
ar[5] = "Salut, ce mai faci?";
console.log(ar[5]);

ar = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

console.log(ar[0][1]);

var ar = [0, 5, 8, 24, 75, "salut", true];

var pos = ar.indexOf("salutt");
var lungime = ar.length;
console.log(lungime);
var inc = ar.includes("82");

ar.push(82);
console.log(ar);
//ar.pop();
//console.log(ar);

for (var i = 0; i <= ar.length - 1; i++) {
  //sau i < ar.length fara -1
  console.log("Elementul " + (i + 1) + " din array este: " + ar[i]);
}

// incepe cu 0, 1; urmatoarele elemente sunt suma celor 2 precedente: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 ... - sirul lui fibonacci

// script primele 10 numere fibonacci:
n1 = 0;
n2 = 1;
nr_urmator = n1 + n2;
var fibonacci = [n1, n2];
for (var i = 2; i < 10; i++) {
  console.log(fibonacci);
  nr_urmator = n1 + n2;
  n1 = n2;
  n2 = nr_urmator;
  fibonacci.push(nr_urmator);
}
console.log(fibonacci);

console.log(" "); // sau

var fib = [0, 1];
for (i = 2; i < 10; i++) {
  fib.push(fib[i - 1] + fib[i - 2]);
}
console.log(fib);

// o functie cu un paramentru de tip array
// - functie care va returna nr de elemente unice din array
// [0, 2, 5, 5, 7, 7, 7, 3]

var a = [0, 2, 5, 5, 7, 7, 3];

function unic(x) {
  var b = [];

  for (i = 0; i < x.length; i++) {
    if (b.includes(x[i]) == false) {
      b.push(x[i]);
    }
  }
  return b.length;
}

console.log(unic(a));

// o functie cu parametru de tip array
// va returna acelasi array inversat
// x=[0, 1, 2, 3]
// inversat(x) -> [3, 2, 1, 0]

var arrr = [0, 1, 2, 3];
// var inversat=a.reverse();
// console.log(inversat)

function ari(a) {
  aa = [];
  for (i = a.length - 1; i >= 0; i--) {
    aa.push(a[i]);
  }
  return aa;
}
console.log(ari(arrr));

// Exercitiul 2
// - o functie care primeste un parametru de tip number
// - functie returneaza true daca nr. e prim, alfel false

function prim(x) {
  var n = true;
  for (i = 2; i <= x / 2; i++) {
    if (x % i == 0) {
      n = false;
    }
  }
  return n;
}
console.log(prim(17));

function prime(x, y) {
  var isPrime;
  var arPrime = [];
  for (var i = x; i <= y; i++) {
    isPrime = true;
    for (var j = 2; j <= i / 2; j++) {
      if (i % j === 0) {
        isPrime = false;
      }
      if (i == 1) {
        isPrime = false;
      }
    }
    if (isPrime == true) {
      arPrime.push(i);
    }
  }
  return arPrime;
}
console.log(prime(1, 50));

/*
a = 121
b = 121
d = un numar nou pe care il formam extragand numere de la finalul numarului b

*/
function palindrom(a) {
  var b = a;
  var c = 0;
  var d = 0;
  while (b > 0) {
    c = b % 10;
    d = d * 10 + c;
    b = parseInt(b / 10);
  }
  console.log(d);
  if (a == d) {
    console.log(a, "este palindrom");
  } else {
    console.log(a, "nu este palindrom");
  }
}
palindrom(12324);

// FUNCTIILE ANONIME

var mai_mare = function () {
  console.log("Este mai mare decat 0.");
};

var egal = function () {
  console.log("Numarul este egal cu 0.");
};

var mai_mic = function () {
  console.log("Numarul este mai mic decat 0.");
};

var x = -5;
if (x > 0) {
  mai_mare();
} else if ((x = 0)) {
  egal();
} else {
  mai_mic();
}

/*
  function arie(forma, a, b, c) {
    var rezultat;
    var pi = 3.14;
    switch (forma) {
      case "triunghi":
        rezultat = (a * b) / 2;
        break;
      case "dreptunghi":
        rezultat = a * b;
        break;
      case "trapez":
        rezultat = ((a + b) * c) / 2;
        break;
      case "cerc":
        rezultat = pi * a * a;
        break;
      default:
        rezultat = "Nu stiu sa calculez aria " + forma;
    }
    return rezultat;
  }
  console.log("Aria triunghiului este: " + arie("triunghi", 6, 6));
  console.log("Aria dreptunghiului este: " + arie("dreptunghi", 6, 6));
  console.log("Aria trapezului este: " + arie("trapez", 6, 6, 6));
  console.log("Aria cercului este: " + arie("cerc", 6));
  */

var triunghi = function (a, b) {
  return (a * b) / 2;
};

var dreptunghi = function (a, b) {
  return a * b;
};

var trapez = function (a, b, c) {
  return ((a + b) * c) / 2;
};

var cerc = function (a) {
  var pi = 3.14;
  return (rezultat = pi * a * a);
};

function rezultat(arie, a, b, c) {
  return arie(a, b, c);
}
// var x = 5;
// var y = 7;
// var z = 10;
console.log(rezultat(dreptunghi, 5, 7, 10));

/* 4 functii abstracte, fiecare cu un parametru(presupunem x)
- dubleaza parametrul ( returneaza x*2 )
- incrementeaza paramentrul ( returneaza x + 1 )
- returneaza parametru scris cu prima litera mare (x = 'salut', returneaza 'Salut')
- returneaza parametru sub forma "==prametru==" ( x = "salut", returneaza "==salut==")
*/
var dublu = function (x) {
  return x * 2;
};
// console.log(dublu(6)) - all good

var increment = function (x) {
  return ++x;
};
// console.log(increment(10)) - all good

var litera_mare = function (x) {
  return x.charAt(0).toUpperCase() + x.slice(1);
};
// console.log(litera_mare("salut")); - all good

var caractere = function (x) {
  return (x = "==" + x + "==");
};
// console.log(caractere("parametru"))  all good

function raspunsuri(functie, x) {
  var b = [];
  for (i = 0; i < x.length; i++) {
    b.push(functie(x[i]));
  }
  return b;
}

var ar_numere = [2, 4, 6];
var ar_string = ["unu", "doi", "trei"];

console.log(raspunsuri(dublu, ar_numere));
console.log(raspunsuri(increment, ar_numere));
console.log(raspunsuri(litera_mare, ar_string));
console.log(raspunsuri(caractere, ar_string));

// var masina = {
//   marca: 'Mercedes',
//   model: 'S Class',
//   an: 2015,
//   km: 150000,
//   launch: function(x){
//     alert('Nu bate, nu troncane! ' + x)
//   }
// }

// console.log(masina.marca);
// masina.launch("JUR");

function Car(marca, model, pretPornire, an, km) {
  this.marca = marca;
  this.model = model;
  this.pretPornire = pretPornire;
  this.an = an;
  this.km = km;
  this.launch = function (x) {
    alert("Nu bate, nu troncane! " + x);
  };

  this.filtruPret = function (x, y) {
    if (this.pretPornire >= x && this.pretPornire <= y) {
      return true;
    } else {
      return false;
    }
  };
}

var masina1 = new Car("Ford", "Focus", 10000, 2020, 80000);
// masina1.launch();
console.log(masina1);
var masina2 = new Car("Volkswagen", "Passat", 10000, 2010, 200000);
console.log(masina1.filtruPret(8000, 12000));
var masina4 = new Car("Lexus", "IS", 20000, 2014, 200000);
var masina5 = new Car("Land", "Rover", 5000, 1990, 250000);
var masini = [];
masini[0] = new Car("BMW", "seria 3", 10000, 2015, 150000);
masini.push(masina1);
masini.push(masina2);
masini.push(masina4);
masini.push(masina5);
console.log(masini[1].marca);
console.log(masini);
masini[5] = new Car("Dacia", "Logan", 4000, 2016, 130000);

// var filtrareMasini = function(masini, pret){
function filtrareMasini(masini, pret) {
  var masiniEftine = [];
  for (i = 0; i < masini.length; i++) {
    if (masini[i].pretPornire < pret) {
      masiniEftine.push(masini[i].marca);
    }
  }
  return masiniEftine;
}

console.log(filtrareMasini(masini, 21000));



function student(numeStudent, prenume, varsta, cStudent) {
  this.numeStudent = numeStudent;
  this.prenume = prenume;
  this.varsta = varsta;
  this.cStudent = cStudent;
  this.BagaCursNou = function(cursnou){
    this.cStudent.push(cursnou);
  }
}

function curs(numeCurs, nrOre, descriere) {
  this.numeCurs = numeCurs;
  this.nrOre = nrOre;
  this.descriere = descriere;
}

// constructor cursuri noi
function cursNou(numeCurs, nrOre, descriere) {
  this.numeCurs = numeCurs;
  this.nrOre = nrOre;
  this.descriere = descriere;
}
// cursurile initiale
var c = [];
c[0] = new curs("Engleza", 10, "Limba Engleza intensiv");
c[1] = new curs("Matematica", 3, "noaptea mintii");
c[2] = new curs("JavaScript", 100, "un fel de CSS");

/* array in care adaug cursuri noi*/
var cursNoiStudent0 = [];
var cursNoiStudent1 = [];
var cursNoiStudent2 = [];
var cursNoiStudent3 = [];

/* array in care sunt toate cursurile studentului, cele initiale + cursuri adaugate noi individual de Student*/
var cursuriStudent0 = c.concat(cursNoiStudent0);
var cursuriStudent1 = c.concat(cursNoiStudent1);
var cursuriStudent2 = c.concat(cursNoiStudent2);
var cursuriStudent3 = c.concat(cursNoiStudent3);

// definire studenti
var studenti = [];
studenti[0] = new student("Fako", "Alexandru", 31, cursuriStudent0);
studenti[1] = new student("Covidiu", "Ovidiu", 80, cursuriStudent1);
studenti[2] = new student("Gica", "Gabi", 81, cursuriStudent2);
studenti[3] = new student("Ion", "Ionescu", 20, cursuriStudent3);

console.log(studenti[0])
// Pas 1 -definire curs nou
// Cursurile noi
var cursNou1 = new curs("filozofie", 5, "prostii");
var cursNou2 = new curs("Franceza", 100, "franceza");
var cursNou3 = new curs("Sport", 100, "fotbal");
var cursNou4 = new curs("CSS1", 10, "Working ");
var cursNou5 = new curs("CSS2", 11, "Working with ");
var cursNou6 = new curs("CSS3", 12, "Working with something");

// Pas 2 -adaugare curs nou la student
// Cursuri noi student 0
cursuriStudent0.push(cursNou1);
cursuriStudent0.push(cursNou2);
cursuriStudent0.push(cursNou6);
// Cursuri noi student 1
cursuriStudent1.push(cursNou4);
cursuriStudent1.push(cursNou3);

// Cursuri noi student 2
cursuriStudent2.push(cursNou4);

// Cursuri noi stundet 3
cursuriStudent3.push(cursNou6);
cursuriStudent3.push(cursNou3);

// console.log(studenti);
// console.log(cursuriStudent3);
console.log(studenti[0].cStudent[0].numeCurs);
// console.log(studenti[0].cStudent);



function studentiComuni(arrayStudenti, CURS) {
  var StudentiComuni = [];
  for (i = 0; i < arrayStudenti.length; i++) {
    for (j = 0; j < arrayStudenti[i].cStudent.length; j++) {
      if (arrayStudenti[i].cStudent[j].numeCurs == CURS) {
        StudentiComuni.push(arrayStudenti[i].numeStudent);
      }
    }
  }
  return StudentiComuni;
}

var cursNouX = new curs("CSSssss", 12, "Working with something");
studenti[1].BagaCursNou(cursNouX);
console.log(studenti);

console.log(studentiComuni(studenti, "CSSssss"));

function Actor(numeActor, varstaActor, Oscar){
  this.numeActor = numeActor;
  this.varstaActor = varstaActor;
  this.Oscar = Oscar;
}

function Film(numeFilm, anAparitie, actoriFilm, notaIMDB, gen){
  this.numeFilm = numeFilm;
  this.anAparitie = anAparitie;
  this.actoriFilm = actoriFilm;
  this.notaIMDB = notaIMDB;
  this.gen = gen;
}

var actori = [];
actori[0] = new Actor('Riz Ahmed', 39, true);
actori[1] = new Actor('Anthony Hopkins', 84, true);
actori[2] = new Actor('Gary Oldman', 63, true);
actori[3] = new Actor('Leonardo DiCaprio', 47, false);
actori[4] = new Actor('Johnny Depp', 58, false);

var filme = [];
filme[0] = new Film('Sound of Metal', 2019, [actori[0]], 7, 'Music');
filme[1] = new Film('The Father', 2020, [actori[1]], 8, 'Mystery');
filme[2] = new Film('Mank', 2020, [actori[2]], 7, 'Comedy');
filme[3] = new Film('Titanic', 1997, [actori[3]], 8, 'Drama');
filme[4] = new Film('Pirates of Caribbean: The Curse of the Black Pearl', 2003, [actori[4]], 8, 'Adventure');

console.log(filme);
console.log(actori);

function FilmeDeOscar(arrayFilme){
  var filmeDeOscar = [];
  for(i = 0; i < arrayFilme.length; i++){
    for(j = 0; j < arrayFilme[i].actoriFilm.length; j++){
      if(arrayFilme[i].actoriFilm[j].Oscar == true){
        filmeDeOscar.push(arrayFilme[i].numeFilm);
      }
    }
  }
  return filmeDeOscar;
}

console.log(FilmeDeOscar(filme));

function FiltrareFilme(arrayFilme, gen, nota){
  var filmeFiltrate = [];
  for (i = 0; i < arrayFilme.length; i++){
    if((arrayFilme[i].gen == gen) && (arrayFilme[i].notaIMDB >= nota)){
      filmeFiltrate.push(arrayFilme[i].numeFilm);
    }
  }
  return filmeFiltrate;
}

console.log(FiltrareFilme(filme, 'Comedy', 6));

var actorX = JSON.stringify(actori[0]);
console.log(actorX);

var studentJSON = JSON.stringify(studenti[0]);
console.log(studentJSON);
var obiectStudJSON = JSON.parse(studentJSON);
console.log(obiectStudJSON);


function factorial(nr){
  if (nr == 1){{
    return 1;
  }
  }
  return nr * factorial(nr - 1);
}

console.log(factorial(5));

// incepe cu 0, 1; urmatoarele elemente sunt suma celor 2 precedente: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 ... - sirul lui fibonacci

// var fib = [0, 1];
// for (i = 2; i < 10; i++) {
//   fib.push(fib[i - 1] + fib[i - 2]);
// }
// console.log(fib);

/*function fibo(nr){
  if (nr == 1){
    return 0;
  }else if(nr == 2){
    return 1;
  }
    return fibo(nr-2) + fibo(nr-1);
  }


console.log(fibo(20));*/


