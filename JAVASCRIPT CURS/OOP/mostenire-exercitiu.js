class Triunghi {
    constructor(a, b, c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    arie = function(){
        // radical(semiperimetru * ( semiperimetru - a) * (semiperimetru - b) * (semiperimetru - c))
        var semiperimetru = this.perimetru()/2;
        console.log(`triunghi arie`)
        return Math.sqrt(semiperimetru * (semiperimetru - this.a) * (semiperimetru - this.b) * (semiperimetru - this.c))
    }
    perimetru = function(){
        console.log(`triunghi perimetru`)
        return this.a + this.b + this.c;
    }
}

const triunghi = new Triunghi(10, 12, 12);
console.log(triunghi)
triunghi.perimetru();
console.log(triunghi.perimetru());
triunghi.arie();
console.log(triunghi.arie());

class Isoscel extends Triunghi {
    constructor(x, y){
        super(x, x, y);
    }
    perimetru = function(){
        console.log(`isoscel permietru`);
        return 2*(this.a) + this.c;
    }
}
const isoscel = new Isoscel(10, 16);
console.log(isoscel.perimetru());
console.log(isoscel.arie());

class Echilateral extends Triunghi {
    constructor(x){
        super(x, x, x);
    }
    perimetru = function(){
        console.log(`echilateral perimetru`);
        return 3*this.a
    }
    arie1 = function(){
        console.log(`echilateral arie`);
        //latura la puterea a 2-a inmultita cu radical din 3, impartit la 4
        return (Math.pow(this.a, 2) * Math.sqrt(3)) /4
    }
}

const echilateral = new Echilateral(5);
console.log(echilateral.perimetru());
console.log(echilateral.arie());
console.log(echilateral.arie1());

class Dreptunghic extends Triunghi {
    constructor(a, b, c){
        super(a, b, c);
    }
    arie = function(){
        console.log(`arie dreptunghic`)
        return this.a * this.b / 2;
    }
}

const dreptunghic = new Dreptunghic(3, 4, 5);
console.log(dreptunghic.arie());

class Dreptunghic_Isoscel extends Isoscel {
    constructor(a, b){
        super(a, b);
    }
    arie = function(){
        console.log(`Arie DreptIsos`);
        return Math.pow(this.a, 2)/2;
    }
}
const dreptunghic_isoscel = new Dreptunghic_Isoscel(3,5);
console.log(dreptunghic_isoscel.arie());
console.log(dreptunghic_isoscel.perimetru());
