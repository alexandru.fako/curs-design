class car{
    constructor(brand, model){
        var _brand = brand;
        this.model = model;
        this.getBrand = function(){
            return _brand;
        }
        this.setBrand = function(newBrand){
            _brand = newBrand;
        }
        this.displayModel = function () {
            console.log(this.model);
        }
        this.displayModel2 = () => {
            console.log(this.model);
        }
    }

    launch = () => {
        console.log("works");
    }
};

var auto = new car("mazda", '323F');
auto.launch();

console.log(auto.brand);
auto.brand = 'ford';
console.log(auto.brand);
console.log(auto.getBrand());
auto.setBrand('volvo');
console.log(auto.getBrand())

//definitie
const a = (b, c) => b + c;
const b = (b, c) => {
    return b + c;
};

console.log( a(1, 2));
console.log( b(1, 2));

//returnare obiect
const x = (b, c) => ({b: b, c: c});
const y = (b, c) => {
    return {b: b,c: c};
};

console.log( x('ceva', 'altceva'));
console.log( y('ceva', 'altceva'))
auto.displayModel();
auto.displayModel2();

function sum(a, b = 5){
    return a + b;
}
var z = sum(2);
console.log(z);
var w = sum(2, 3);
console.log(w);
var j = sum(3);
console.log(j);

//functia de mai sus ca ARROW
const sum2 = (a, b = 5) => a + b; // {return a + b + c;}

var arr = [10, 11, 12, 30];

for(let i=0; i<arr.length; i++) {
    console.log(arr[i]);
}

arr.forEach((value, index, array) => {
    console.log(value); // corespunde cu valoarea din array
    console.log(index); // pozitia din array, i intr-un for cu arr.length
    console.log(array); // arrayul - inutil
    if(value === 12) {
        console.log('success');
    }
})

console.log(arr.find((val) => {
    if(val % 2 === 1){
        return true;
    }
    return val === 31; //la misto
}));

console.log(arr.filter((value) => {
    if(value % 2 === 0) { //if(!(value % 2))
        return true;
    }
}))

console.log(arr.filter((value) => {
    if(value === 4) {
        return true;
    }
}));

console.log(arr.filter((value) => value === 4));

var name = 'John';
var surname = 'Doe';
var age = 22;

var text = 'My name is ' + name + ' ' + surname + '. I am ' + age + ' years old.';
console.log(text);

var text2 = `My name is ${name} ${surname}. I am ${age + 4} years old.`;
console.log(text2);

var text = `first line
second line
third line`;

var text = 'first \' line \n' + 'second line \n' + 'third line';
console.log(text);
