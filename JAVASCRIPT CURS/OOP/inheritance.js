class Animal{
    constructor(name, size) {
        this.name = name;
        this.size = size;
    }
    hello() {
        console.log(this.name);
    }
}

const animal = new Animal('caine', 132);
animal.hello();

//thechild class
class Bird extends Animal{
    constructor(name, size, speed) {
        super(name, size);  
        this.speed = speed;
    }
    hello() {
        console.log(`i am  ${this.name}!`);
    }
}
//creating and using the object
var bird = new Bird("sparrow", 99, 100);
bird.hello();

class Mammal extends Animal {
    constructor(name, size){
        super(name, size);
    }
}

const mammal = new Mammal(`pisica`, 15);
mammal.hello();

class Eagle extends Bird {
    constructor(name, size, speed, beak){
        super(name, size, speed);
        this.beak = beak;
    }
}

const eagle = new Eagle(`plesuv`, 30, 100, true);
eagle.hello()