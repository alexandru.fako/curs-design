/*
Define a Book class which contains the information:

a title,
an author,
the year it was published ,
the number of pages ,
is it currently reserved.


 */


class Book {
    constructor(title, author, year, nrPages, reserved) {
        this.title = title;
        this.author = author;
        this.year = year;
        this.nrPages = nrPages;
        this.reserved = reserved;
    }
    hello () {
        console.log(`${this.title} by ${this.author} is reserved?: ${this.reserved}`)
    }
}

const book1 = new Book(`Cartea Junglei`, `Rudyard Kipling`, 1894, 176, false)
const book2 = new Book(`Jurnalul`, `Bendeac`, 2018, 300, true);
const book3 = new Book (`Harry Potter`, `J.K. Rowling`, 1997, 351, true);
const book4 = new Book (`5 AM`, `Robert Sharma`, 2000, 400, true);

book1.hello()

class Library {
    constructor(name, address, books) {
        this.name = name;
        this.address = address;
        this.books = books;
    }
    addBook = function(title, author, year, nrPages, reserved) {
        this.books.push(new Book(title, author, year, nrPages, reserved))
    }
    addBook2 = function(book) {
        this.books.push(book);
    }
    searchBook = function(titlu){
        var foundBook = this.books.find((value) => {
            
            return value.title == titlu;
        })
        if(foundBook != undefined){
            console.log(`Avem cartea!`)
        }else{
            console.log(`NU!`)
        }
    }

    searchBun = function(titlu){
        var foundBook = this.books.find((value) => {
            
            return value.title == titlu;
        })
        return foundBook;
    }

    searchBook2 = function(titlu){
        var foundBook = this.books.find((value) => value.title == titlu)
        if(foundBook != undefined){
            console.log(`Avem cartea!`)
        }else{
            console.log(`NU!`)
        }
    }

    reserve = function(titlu){
        var cauta = this.searchBun(titlu);
        if(cauta != undefined){
            if(cauta.reserved == true){
                cauta.reserved = false
            }else{
                cauta.reserved = true
            }
        }else{
            console.log(`Cartea \"${titlu}\" nu poate fi rezervata`)
        }
    }

    deleteBook = function(sters0){
        var foundBook = this.books.findIndex((sters) => {
            
            return sters.title == sters0;
        })
        if( foundBook != -1){
            this.books.splice(foundBook, 1);
        }else{
            console.log(`Cartea nu exista!`);
        }
    }
}

const library = new Library(`Libraria din beci`, `in beci`, [book1, book2, book3, book4]);
console.log(library);

//const library = new Library(`Libraria din beci`, `in beci`, [new Book(...param), new Book(...param)]);
/*
arr = [book1, book2,...]
arr = [new Book(...param), new Book(...param)]
const library = new Library(`Libraria din beci`, `in beci`, arr[]);
*/

arr = [new Book(`x`, `x`, 3, 4, true), new Book(`x`, `x`, 3, 4, true)];
console.log(arr);
console.log(library.books);

library.addBook(`carte`, `autor`, 1999, 299, false);

library.addBook2(new Book(`carte2`, `autor2`, 1999, 299, false));
library.addBook2(book1);

console.log(library.books)


const library2 = new Library(`Librarie2`, `adresa`, []);
console.log(library2);
library2.addBook(`carte`, `autor`, 1999, 299, false);
console.log(library2);
console.log(library);
library.searchBook(`carte`);
library.searchBook(`cart`);
library.searchBook2(`cart`);
console.log(library);
library.reserve(`carte`);
console.log(library);
library.reserve(`carttt`);

library.deleteBook(`5AM`);
console.log(library);