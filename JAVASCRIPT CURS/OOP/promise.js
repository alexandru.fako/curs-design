//const promise = new Promise((resolve, reject) => {})
const promise = new Promise(function(resolve, reject){
    setTimeout(() => {
        const status = true;
        if(status) {
            resolve(`resolve`);
        } else {
            reject(`reject`);
        }
    }, 2000)
    
});

console.log(`start`);
console.time(`timer`);
promise.then(
    (result) => {
        console.log(result);
        console.timeEnd(`timer`);
    },
    (error) => {
        console.log(`error:`, error);
        console.timeEnd(`timer`);
    } 
);

/* 
console.log(`end`);
setTimeout(() => {
    console.log(`setTimeout 0`);
}, 0);

setTimeout(() => {
    console.log(`setTimeout`);
});

                          EXERCITIU PROMISE                             
Your function must always return a promise
If data is not a number, return a promise rejected instantly and give the data "error" (in a string)
If data is an odd number, return a promise resolved 1 second later and give the data "odd" (in a string)
If data is an even number, return a promise rejected 2 seconds later and give the data "even" (in a string) */

function job(data) {
    const promise = new Promise((suc, err) => {
        if(isNaN(data)){
            err("error");
        }else if(data % 2 == 1){
            setTimeout(() =>
            suc("odd"), 1000)
        }else if(data%2 == 0){
            setTimeout(() =>
            err("even"), 2000)
        }
    })
    return promise;
}


job(`test`)
.then((succes) => {
    console.log(`test failed`)
}, 
    (error) => {
        if (error === `error`) {
           console.log(`succes`)
        }else{
            console.log(`zfailed`)
        }
    console.log(`test succes`)
}
);

job(3)
.then((succes) => {
    console.log(`test succes`)
}, 
    (error) => {
    console.log(`test failed`)
}
);

job(4)
.then((succes) => {
    console.log(`test failed`)
}, 
    (error) => {
    console.log(`test succes`)
}
);


