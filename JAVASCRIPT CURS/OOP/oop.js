var movie = {
    title: 'Titanic',
    year: 1997,
    director: 'James Cameron',
    info: function(){
        console.log(this.title + " by " + this.director);
    },
    info2: function(){
        return this.title + ' ' + this.director;
    }
};

console.log(movie.title);
console.log(movie.year);
movie.year = 0;
console.log(movie.year);
console.log(movie.info2());     //afiseaza
movie.info();                   //returneaza

function Movie(title, year, director) {
    this.title = title;
    this.year = year;
    this.director = director;
    this.info = function(){
        console.log(this.title + " by " + this.director);
    };
};

var m1 = new Movie('Gladiator', 2000, 'Ridley Scott');
var m2 = new Movie('The Last Samurai', 2003, 'Edward Zwick');

var m4 = Object.create(Movie);  //var m4 = {}

m1.info();
m2.info();
console.log(m4.title);

console.log(movie instanceof Object);
console.log(movie instanceof Movie);

var string = JSON.stringify(movie);
console.log(string);
console.log(string instanceof Object);
var obj = JSON.parse(string);
console.log(obj);
console.log(obj instanceof Object);

var string2 = new String('orice text');
console.log(string2);
console.log(string2 instanceof Object);

function constructor(a, b){
    return a + b;
}

console.log(constructor(1,2));