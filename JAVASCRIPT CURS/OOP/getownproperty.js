const object = {
    id: 1, name: 'nume1', x: 3
 }
 
 const object2 = {
    id: 1, description: 'nume1', x: 3
 }
 
 console.log(object.id)
 console.log(object.name)
 
 console.log(object[`id`]);
 console.log(`string:  ${object[`name`]}`);
 
 // console.log(nume_obiect.hasOwnProperty(nume_proprietate));
 console.log(object.hasOwnProperty("idsadasdas"));
 
 // console.log(Object.getOwnPropertyNames(nume_obiect));
 console.log(Object.getOwnPropertyNames(object));
 console.log(Object.getOwnPropertyNames(object2));
 
 const objectKeys = Object.getOwnPropertyNames(object);
 
 objectKeys.forEach((key) => {
    console.log(`key: ${key}`);
    console.log(`value: ${object[key]}`) // object["id"]
    console.log(`value: ${object.key}`)
 })

 console.log(`for`)

 for(i=0; i < objectKeys.length; i++){
     console.log(`key: ${objectKeys[i]}`)
     console.log(`value: ${object[objectKeys[i]]}`)
     const key = objectKeys[i];
     console.log(`value: ${object[key]}`)
 }