class Car {
    constructor(brand, model, year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
}

const car = new Car('Mazda', '323f', 1997);
const car1 = new Car('Volvo', 'XC90', 2020);

console.log(car);

const m = car.model;
const b = car.brand;

console.log(m);
console.log(b);

const {brand, model, year, speed} = car; // cauta in interiorul obiectului proprietatile respective si le atribuie in variabila denumita

console.log(brand);
console.log(model);
console.log(year);

const obiect = {brand: 'Mercedes', model: 'CLS350', yearr: 2002, topspeed: 200};
const {yearr, topspeed} = obiect;
console.log(yearr);
console.log(topspeed);

const {brand: br, model: mo, year: ye, speed: s} = car1; 
/*  
    const br = car1.brand;
    const mo = car1.model;
    const s = car1.speed; */ 

console.log(br);
console.log(mo);
console.log(ye);

const arr = [10, 11, 12, 30];
const [a, c] = arr; //destructuring pe array va lua elementele in ordinea pozitiei din array
console.log(a);
console.log(c);
