//const promise = new Promise((resolve, reject) => {})
const promise = new Promise(function(resolve, reject){
    setTimeout(() => {
        const status = false;
        if(status) {
            resolve(`resolve`);
        } else {
            reject(`reject`);
        }
    }, 2000)
    
});

async function resolvePromise() { 
    const result = (await promise); //await e doar pentru resolve
    console.log(result);
 };

 resolvePromise();

 async function resolvePromise() {
     try {
         console.log(await promise);
     } catch(error) {
         console.log(`error:`, error);
     }
 }

