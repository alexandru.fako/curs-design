var movie = function(newTitle, newDirector){
	let title = newTitle;
	let director = newDirector;
	this.info = function(){
		console.log(title + " by " + director);
	}


//getter
this.getTitle = function() {
    return title;
}
//setter
this.setTitle = function(newTitle){
    title = newTitle;
}
};
var m1 = new movie("titanic", 'James Cameron');

m1.info();
m1.title = 3;
m1.info();

m1.setTitle('Doom');
m1.info();
console.log(m1.getTitle());

