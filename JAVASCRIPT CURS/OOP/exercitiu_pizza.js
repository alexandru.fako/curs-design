class Pizza {
  constructor(faina, sare, apa, drojdie, pret, timp_preparare) {
    this.faina = faina;
    this.sare = sare;
    this.apa = apa;
    this.drojdie = drojdie;
    this.pret = pret;
    this.timp_preparare = timp_preparare;
    this.ingredienteExtra = [];
  }
  info = function () {
    const proprietatiPizza = Object.getOwnPropertyNames(this);
    // console.log(proprietatiPizza);
    proprietatiPizza.forEach((ingredient) => {
        if(typeof this[ingredient] != `function`){
          console.log(`${ingredient} : ${this[ingredient]}`)}
    })
    console.log(); //console.log implicit face o linie noua => linie noua
  };

  coacePizza = function(){
    const promise = new Promise((result, reject) => {
      setTimeout(() => {
        const random = Math.random()
        if(random<0.5){
          result(`S-a copt`)
        }else{
          reject(`S-a prajit de tot`)
        }
      }, this.timp_preparare * 1000)
    });

    promise.then(
      (result) => {
        console.log(result);
  },
      (error) => {
        console.log(`error:`, error);
  })
  }

  adaugaIngrediente = function (ingredient) {
      this.ingredienteExtra.push(ingredient);
  }
}
const pizza = new Pizza(100, 10, 50, 101, `60 de lei`, 5);
pizza.info();

class PizzaMare extends Pizza {
  constructor(faina, sare, apa, drojdie, pret) {
    super(faina, sare, apa, drojdie, pret, 6);
  }
}

const pizza_mare = new PizzaMare(200, 15, 100, 20, `80 de lei`);
pizza_mare.info();

class PizzaMica extends Pizza {
  constructor(faina, sare, apa, drojdie, pret) {
    super(faina, sare, apa, drojdie, pret, 3);
  }
}

const pizza_mica = new PizzaMica(75, 5, 40, 10, `40 de lei`);
pizza_mica.info();

class Margherita_mare extends PizzaMare {
  constructor(faina, sare, apa, drojdie, pret, sosRosii, mozzarela, busuioc) {
    super(faina, sare, apa, drojdie, pret);
    this.sosRosii = sosRosii;
    this.mozzarela = mozzarela;
    this.busuioc = busuioc;
  }
}

const marguerita_mare = new Margherita_mare(
  200,
  100,
  156,
  61,
  `80 de lei`,
  70,
  135,
  `10 frunze`
);
marguerita_mare.info();

class Margherita_mica extends PizzaMica {
  constructor(faina, sare, apa, drojdie, pret, sosRosii, mozzarela, busuioc) {
    super(faina, sare, apa, drojdie, pret);
    this.sosRosii = sosRosii;
    this.mozzarela = mozzarela;
    this.busuioc = busuioc;
  }
}

const marguerita_mica = new Margherita_mica(
  100,
  50,
  75,
  61,
  `60 de lei`,
  35,
  135,
  `10 frunze`
);
marguerita_mica.info();

class Diavola_mare extends PizzaMare {
  constructor(
    faina,
    sare,
    apa,
    drojdie,
    pret,
    sosRosii,
    mozzarela,
    busuioc,
    peperoni
  ) {
    super(faina, sare, apa, drojdie, pret);
    this.sosRosii = sosRosii;
    this.mozzarela = mozzarela;
    this.busuioc = busuioc;
    this.peperoni = peperoni;
  }
}

const diavola_mare = new Diavola_mare(
  200,
  100,
  156,
  61,
  "80 de lei",
  70,
  135,
  "3 frunze",
  1000
);
diavola_mare.info();
class Diavola_mica extends PizzaMica {
  constructor(
    faina,
    sare,
    apa,
    drojdie,
    pret,
    sosRosii,
    mozzarela,
    busuioc,
    peperoni
  ) {
    super(faina, sare, apa, drojdie, pret);
    this.sosRosii = sosRosii;
    this.mozzarela = mozzarela;
    this.busuioc = busuioc;
    this.peperoni = peperoni;
  }
}

const diavola_mica = new Diavola_mica(
  100,
  50,
  76,
  43,
  "50 de lei",
  30,
  64,
  "1.3 frunze",
  460
);
diavola_mica.info();

class QuatroS_mare extends PizzaMare {
  constructor(
    faina,
    sare,
    apa,
    drojdie,
    pret,
    sosRosii,
    masline,
    salam,
    porumb,
    ciuperci
  ) {
    super(faina, sare, apa, drojdie, pret);
    this.sosRosii = sosRosii;
    this.masline = masline;
    this.salam = salam;
    this.porumb = porumb;
    this.ciuperci = ciuperci;
  }
}

const quatros_mare = new QuatroS_mare(
  200,
  100,
  156,
  61,
  "80 de lei",
  340,
  241,
  870,
  300,
  140
);
quatros_mare.info();

class QuatroS_mica extends PizzaMica {
  constructor(
    faina,
    sare,
    apa,
    drojdie,
    pret,
    sosRosii,
    masline,
    salam,
    porumb,
    ciuperci
  ) {
    super(faina, sare, apa, drojdie, pret);
    this.sosRosii = sosRosii;
    this.masline = masline;
    this.salam = salam;
    this.porumb = porumb;
    this.ciuperci = ciuperci;
  }
}

const quatros_mica = new QuatroS_mica(
  100,
  50,
  76,
  31,
  "40 de lei",
  170,
  121,
  430,
  150,
  70
);
quatros_mica.adaugaIngrediente(`sardine`);
quatros_mica.adaugaIngrediente(`oregano`);
quatros_mica.info();

diavola_mare.coacePizza();
