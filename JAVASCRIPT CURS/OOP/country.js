class City{
    constructor (name, population, description){
        this.name = name;
        this.population = population;
        this.description = description;
    }
    info = function(){
        console.log(`City of ${this.name} with population of ${this.population} . ${this.description}`)
    }
};

const city1 = new City(`Constanta`, 1250000, `la mare ca baietii`);
const city2 = new City('Schela', 1000000, 'A beautiful village where you can observe drunk people in their natural habitats');
city1.info();