const x = [{
    id: 1, name: `nume1`
}, {
    id: 2, name: `nume2`
}, {
    id: 3, name: `nume3`
}, {
    id: 4, name: `nume4`
}, {
    id: 5, name: `nume5`
}, ];

console.log(x);
// push add element(s) at the end
let y = x.push({id: 6, name: 'nume6'});
// let y = x.length;
console.log(x);
console.log(y);

// pop removes element from the end
y = x.pop();
console.log(x);
console.log(y);

// shift removes element from the start
y = x.shift();
console.log(x);
console.log(y);

// unshift add element(s) at the start
y = x.unshift({id: 0, name: 'nume0'});
console.log(x);
console.log(y);

// slice returns elements between start(index) and end, start included, end excluded
y = x.slice(1, 3);
console.log(x);
console.log(y);

// splice deletes X elements from the point where it starts  
y = x.splice(1, 1, [`x`, `y`, `z`]);
console.log(x);
console.log(y);

const obj = {
    id: 1, name: `nume1`
}

const newArray = [{
    id: 1, name: `nume1`
}, {
    id: 2, name: `nume2`
}, {
    id: 3, name: `nume3`
}, {
    id: 4, name: `nume4`
}, {
    id: 5, name: `nume5`
},]

for (let i of newArray) { //ia valoarea fiecarui element din array
    console.log(i)
}

for (let i in newArray) { //returneaza indexul fiecarui element din array
    console.log(i)
}

for (let i in obj) { //returneaza cheile din obiect, adica proprietatile obiectului
    console.log(i)
}

